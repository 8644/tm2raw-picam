from williamyeh/java8
RUN mkdir -p /picam
RUN mkdir /picam/spice
RUN mkdir /picam/data
WORKDIR /picam
COPY tm2raw.tar.gz /picam/
RUN tar -zxvf /picam/tm2raw.tar.gz
RUN rm -f /picam/tm2raw.tar.gz
COPY tm2raw.properties /picam/tm2raw/conf/
COPY raw2cal.properties /picam/tm2raw/conf/
RUN apt-get update
RUN apt-get -y install nano



#to create the container with host directories mapped to container directories do following:
#copy spice kernel directory to d:/docker/spice
#docker run --rm -v /path/to/filetree/root:/picam/data/ -v /path/to/spice/kernels:/picam/spice -it [image] bash
#docker run --rm -v /path/to/filetree/root:/picam/data/ -v /path/to/spice/kernels:/picam/spice fginer/tm2raw-picam:latest /picam/tm2raw/bin/tm2raw-picam
#docker run --rm -v /d/temp/tm2raw:/picam:data/ -v /d/docker-data/spice/kernels:/picam/spice fginer/tm2raw-picam:latest /picam/tm2raw/bin/tm2raw-picam